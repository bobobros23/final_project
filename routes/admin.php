<?php

Route::get('/dashboard', 'Admin\PageController@index')->name('dashboard');

//category
Route::get('/category', 'Admin\CategoryController@index')->name('category');
Route::get('/category/add', 'Admin\CategoryController@add')->name('add.category');